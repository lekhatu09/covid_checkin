import Vue from 'vue'
import VueRouter from 'vue-router'
import IntroSite from '../views/Intro/IntroSite.vue'
import LocationCountry from '../views/Location/LocationCountry.vue'
import OfficeGuidelines from '../views/OfficeGuidelines/OfficeGuidelines'
import HealthChecklist from '../views/HealthChecklist/HealthChecklist'
import PersonalInformation from '../views/PersonalInformation/PersonalInformation'
import ResultSuccessfully from '../views/Result/ResultSuccessfully'
import ResultFailed from '../views/Result/ResultFailed'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'introSite',
    component: IntroSite,

  },
  {
    path: '/location',
    name: 'location',
    component: LocationCountry,

  },
  {
    path: '/office',
    name: 'office',
    component: OfficeGuidelines,

  },
  {
    path: '/health',
    name: 'health',
    component: HealthChecklist,
  },
  {
    path: '/contact',
    name: 'contact',
    component: PersonalInformation,
  },
  {
    path: '/success',
    name: 'success',
    component: ResultSuccessfully,
  },
  {
    path: '/failed',
    name: 'failed',
    component: ResultFailed,
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

import state from './state'
import getters from './getter'
import mutations from './mutations'
import actions from './actions'

const storeConfigs = {
    state,
    getters,
    mutations,
    actions,
}

export default storeConfigs;
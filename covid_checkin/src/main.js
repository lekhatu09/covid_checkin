import Vue from 'vue'
import App from './App.vue'

import vuetify from './plugins/vuetify'
import router from './router'
import Vuex from 'vuex'

import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';

Vue.use(Vuex)
import storeConfigs from './store'
const store = new Vuex.Store(storeConfigs)

Vue.config.productionTip = false


Vue.component('vue-phone-number-input', VuePhoneNumberInput);


// const store = new Vuex.Store({
//   state: {
//     name: 'Test 123'
//   },
//   getters: {
//     names: state => state.name 
//     //lay name tu state
//   },
//   mutations: {
//     setName(state,namess) {
//       state.name = namess;
//     }
//   },
//   actions: {
//     updateProfile({commit}) {
//     commit('setName','testaaaaa')
//     }
//     // updateProfile(context) {
//     // context.commit('setName','testaaaaa')
//     // }
//   }
//   //context la store o trang thai hien tai
// })

new Vue({
  vuetify,
  router,
  render: h => h(App),
  store
}).$mount('#app')
